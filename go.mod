module gitlab.com/dewadg/sadalsuud

go 1.15

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.1.1
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	gitlab.com/dewadg/heda v1.5.0
	golang.org/x/text v0.3.2
)
