package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"log"

	"github.com/joho/godotenv"
	"github.com/spf13/cobra"
	"gitlab.com/dewadg/sadalsuud/cmd/serve"
)

var rootCmd = &cobra.Command{
	Use: "sadalsuud",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Please specify a command")
	},
}

func init() {
	godotenv.Load()
}

func main() {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})

	rootCmd.AddCommand(serve.Command(logger))

	if err := rootCmd.Execute(); err != nil {
		log.Fatalln(err)
	}
}
