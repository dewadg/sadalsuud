-- +goose Up
-- +goose StatementBegin
CREATE TABLE `crypto_watch_list` (
  pair VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS `crypto_watch_list`;
-- +goose StatementEnd
