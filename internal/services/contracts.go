package services

import (
	"context"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type MessageHandler func(ctx context.Context, message *tgbotapi.Message) error

type BotService interface {
	Listen(ctx context.Context) error

	On(command string, handler MessageHandler)
}
