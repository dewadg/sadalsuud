package services

import (
	"context"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
)

type botService struct {
	logger   logrus.FieldLogger
	bot      *tgbotapi.BotAPI
	handlers map[string][]MessageHandler
}

func NewBotService(logger logrus.FieldLogger, bot *tgbotapi.BotAPI) BotService {
	return &botService{
		logger:   logger,
		bot:      bot,
		handlers: make(map[string][]MessageHandler),
	}
}

func (svc *botService) Listen(ctx context.Context) error {
	cfg := tgbotapi.NewUpdate(0)
	cfg.Timeout = 60

	updateChan, err := svc.bot.GetUpdatesChan(cfg)
	if err != nil {
		svc.logger.WithError(err).Error("Update channel init error")
		return err
	}

	go func() {
		for {
			select {
			case <-ctx.Done():
				svc.logger.Info("Update channel stopped")
				return
			case update := <-updateChan:
				if update.Message == nil {
					continue
				}
				if basicHandlers, ok := svc.handlers["message"]; ok {
					for _, handler := range basicHandlers {
						handler(ctx, update.Message)
					}
				}

				if string(update.Message.Text[0]) == "@" {
					update.Message.Text = strings.Join(strings.Split(update.Message.Text, " ")[1:], " ")
				}

				splittedText := strings.Split(update.Message.Text, " ")
				if handlers, ok := svc.handlers[splittedText[0]]; ok {
					for _, handler := range handlers {
						handler(ctx, update.Message)
					}
				} else {
					svc.sendTextMessage(update.Message.Chat.ID, "Invalid command")
				}
			}
		}
	}()

	return nil
}

func (svc *botService) sendTextMessage(chatID int64, message string) {
	msg := tgbotapi.NewMessage(chatID, message)
	if _, err := svc.bot.Send(msg); err != nil {
		svc.logger.WithError(err).Error("sending_message_error")
	}
}

func (svc *botService) On(command string, handler MessageHandler) {
	_, exists := svc.handlers[command]
	if !exists {
		svc.handlers[command] = []MessageHandler{handler}
	} else {
		svc.handlers[command] = append(svc.handlers[command], handler)
	}
}
