package miner

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type indodaxTickerResponse struct {
	Data struct {
		StringLastPrice string    `json:"last"`
		LastPrice       int64     `json:"-"`
		UnixServerTime  int64     `json:"server_time"`
		ServerTime      time.Time `json:"-"`
	} `json:"ticker"`
}

func getIndodaxTicker(client *http.Client, cryptoCode string, currencyCode string) (indodaxTickerResponse, error) {
	url := fmt.Sprintf("https://indodax.com/api/ticker/%s%s", strings.ToLower(cryptoCode), strings.ToLower(currencyCode))
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return indodaxTickerResponse{}, err
	}

	response, err := client.Do(request)
	if err != nil {
		return indodaxTickerResponse{}, err
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		return indodaxTickerResponse{}, fmt.Errorf("%d: indodax_call_failed_with_status_code", response.StatusCode)
	}

	responseBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return indodaxTickerResponse{}, err
	}

	var responseBody indodaxTickerResponse
	if err := json.Unmarshal(responseBytes, &responseBody); err != nil {
		return indodaxTickerResponse{}, err
	}

	price, err := strconv.Atoi(responseBody.Data.StringLastPrice)
	if err != nil {
		return indodaxTickerResponse{}, err
	}

	responseBody.Data.LastPrice = int64(price)
	responseBody.Data.ServerTime = time.Unix(responseBody.Data.UnixServerTime, 0)

	return responseBody, nil
}
