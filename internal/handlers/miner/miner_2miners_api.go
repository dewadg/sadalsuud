package miner

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type twoMinersStatsResponse struct {
	TwentyFourHoursReward int64                      `json:"24hreward"`
	AvgHashRate           int32                      `json:"hashrate"`
	PaymentsTotal         int64                      `json:"paymentsTotal"`
	Stats                 twoMinersStats             `json:"stats"`
	UnixUpdatedAt         int64                      `json:"int64"`
	UpdatedAt             time.Time                  `json:"-"`
	Rewards               []twoMinersReward          `json:"rewards"`
	SumRewards            []twoMinersSumReward       `json:"sumrewards"`
	Workers               map[string]twoMinersWorker `json:"workers"`
	WorkersTotal          int32                      `json:"workersTotal"`
	WorkersOnline         int32                      `json:"workersOnline"`
	WorkersOffline        int32                      `json:"workersOffline"`
}

type twoMinersStats struct {
	Balance         int64     `json:"balance"`
	ImmatureBalance int64     `json:"immature"`
	UnixLastShare   int64     `json:"lastShare"`
	LastShare       time.Time `json:"-"`
}

type twoMinersReward struct {
	Reward        int64     `json:"reward"`
	Percentage    float32   `json:"percent"`
	Immature      bool      `json:"immature"`
	Orphan        bool      `json:"orphan"`
	Uncle         bool      `json:"uncle"`
	UnixTimestamp int64     `json:"timestamp"`
	Timestamp     time.Time `json:"-"`
}

type twoMinersSumReward struct {
	Reward int64  `json:"reward"`
	Count  int    `json:"numreward"`
	Name   string `json:"name"`
}

type twoMinersWorker struct {
	UnixLastBeat    int64     `json:"lastBeat"`
	LastBeat        time.Time `json:"-"`
	CurrentHashRate int32     `json:"hr"`
	AvgHashRate     int32     `json:"hr2"`
	IsOffline       bool      `json:"offline"`
}

func getTwoMinersStats(client *http.Client, cryptoCode string, accountNumber string) (twoMinersStatsResponse, error) {
	url := fmt.Sprintf("https://%s.2miners.com/api/accounts/%s", cryptoCode, accountNumber)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return twoMinersStatsResponse{}, err
	}

	response, err := client.Do(request)
	if err != nil {
		return twoMinersStatsResponse{}, err
	}
	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		return twoMinersStatsResponse{}, fmt.Errorf("%d: 2miners_call_failed_with_status_code", response.StatusCode)
	}

	responseBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return twoMinersStatsResponse{}, err
	}

	var responseBody twoMinersStatsResponse
	if err := json.Unmarshal(responseBytes, &responseBody); err != nil {
		return twoMinersStatsResponse{}, err
	}

	responseBody.UpdatedAt = time.Unix(responseBody.UnixUpdatedAt, 0)
	responseBody.Stats.LastShare = time.Unix(responseBody.Stats.UnixLastShare, 0)
	for i := range responseBody.Rewards {
		responseBody.Rewards[i].Timestamp = time.Unix(responseBody.Rewards[i].UnixTimestamp, 0)
	}
	for worker, stats := range responseBody.Workers {
		stats.LastBeat = time.Unix(stats.UnixLastBeat, 0)

		responseBody.Workers[worker] = stats
	}

	return responseBody, nil
}
