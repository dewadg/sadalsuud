package miner

import (
	"context"
	"fmt"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
	"net/http"
	"strings"
	"sync"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

var (
	queryOverallStats      = "/mnOverallStats"
	queryDailyStats        = "/mnDailyStats"
	queryRecentRewards     = "/mnRecentRewards"
	queryCumulativeRewards = "/mnCumulativeRewards"
	queryWorkerStats       = "/mnWorkerStats"
)

func miningList(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, miningList map[string][]string) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		index := 1
		for cryptoCode, accounts := range miningList {
			for _, account := range accounts {
				text := fmt.Sprintf("%d. *%s* %s", index, strings.ToUpper(cryptoCode), account)
				data := fmt.Sprintf(" %s:%s", cryptoCode, account)

				msg := tgbotapi.NewMessage(message.Chat.ID, text)
				msg.ParseMode = tgbotapi.ModeMarkdown
				msg.ReplyMarkup = tgbotapi.InlineKeyboardMarkup{
					InlineKeyboard: [][]tgbotapi.InlineKeyboardButton{
						{
							{
								Text:                         "Overall Stats",
								SwitchInlineQueryCurrentChat: strPtr(queryOverallStats + data),
							},
						},
						{
							{
								Text:                         "Recent Rewards",
								SwitchInlineQueryCurrentChat: strPtr(queryRecentRewards + data),
							},
							{
								Text:                         "Cumulative Rewards",
								SwitchInlineQueryCurrentChat: strPtr(queryCumulativeRewards + data),
							},
						},
						{
							{
								Text:                         "Workers Stats",
								SwitchInlineQueryCurrentChat: strPtr(queryWorkerStats + data),
							},
						},
					},
				}

				_, err := bot.Send(msg)
				if err != nil {
					logger.WithError(err).Error("miner.miningList")
				}

				index++
			}
		}

		return nil
	}
}

func strPtr(value string) *string {
	return &value
}

func miningOverallStats(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, client *http.Client) services.MessageHandler {
	printer := message.NewPrinter(language.English)

	return func(ctx context.Context, message *tgbotapi.Message) error {
		splitText := strings.Split(message.Text, " ")
		cryptoCode, accountNumber := parseMiningPair(splitText[1])
		cryptoCode = strings.ToUpper(cryptoCode)
		currencyCode := "IDR"

		wg := sync.WaitGroup{}
		twoMinersStatsChan := make(chan twoMinersStatsResponse, 1)
		indodaxTickerChan := make(chan indodaxTickerResponse, 1)
		errChan := make(chan error, 2)

		wg.Add(1)
		go func() {
			defer wg.Done()

			stats, err := getTwoMinersStats(client, cryptoCode, accountNumber)
			if err != nil {
				msg := tgbotapi.NewMessage(message.Chat.ID, err.Error())
				_, sendErr := bot.Send(msg)
				if sendErr != nil {
					logger.WithError(sendErr).Error("miner.miningOverallStats")
				}

				errChan <- err
				return
			}

			twoMinersStatsChan <- stats
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()

			ticker, err := getIndodaxTicker(client, cryptoCode, currencyCode)
			if err != nil {
				msg := tgbotapi.NewMessage(message.Chat.ID, err.Error())
				_, sendErr := bot.Send(msg)
				if sendErr != nil {
					logger.WithError(sendErr).Error("miner.miningOverallStats")
				}

				errChan <- err
				return
			}

			indodaxTickerChan <- ticker
		}()

		wg.Wait()
		if len(errChan) > 0 {
			return <-errChan
		}
		stats := <-twoMinersStatsChan
		ticker := <-indodaxTickerChan

		text := strings.ReplaceAll(overallStatsMessage, "{cryptoCode}", cryptoCode)
		text = strings.ReplaceAll(text, "{currencyCode}", currencyCode)
		text = strings.ReplaceAll(text, "{24hReward}", fmt.Sprintf("%f", convertBalance(stats.TwentyFourHoursReward)))
		text = strings.ReplaceAll(text, "{24hRewardIDR}", printer.Sprintf("%.0f", convertBalance(stats.TwentyFourHoursReward * ticker.Data.LastPrice)))
		text = strings.ReplaceAll(text, "{unpaidBalance}", fmt.Sprintf("%f", convertBalance(stats.Stats.Balance)))
		text = strings.ReplaceAll(text, "{unpaidBalanceIDR}", printer.Sprintf("%.0f", convertBalance(stats.Stats.Balance * ticker.Data.LastPrice)))
		text = strings.ReplaceAll(text, "{immatureBalance}", fmt.Sprintf("%f", convertBalance(stats.Stats.ImmatureBalance)))
		text = strings.ReplaceAll(text, "{immatureBalanceIDR}", printer.Sprintf("%.0f", convertBalance(stats.Stats.ImmatureBalance * ticker.Data.LastPrice)))
		text = strings.ReplaceAll(text, "{paidBalance}", fmt.Sprintf("%f", convertBalance(stats.PaymentsTotal)))
		text = strings.ReplaceAll(text, "{paidBalanceIDR}", printer.Sprintf("%.0f", convertBalance(stats.PaymentsTotal * ticker.Data.LastPrice)))
		text = strings.ReplaceAll(text, "{lastShare}", stats.Stats.LastShare.In(currentTimezone).Format(time.RFC1123))

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err := bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("miner.miningOverallStats")
		}

		return nil
	}
}

func parseMiningPair(value string) (string, string) {
	pair := strings.Split(value, ":")

	return pair[0], pair[1]
}

func convertBalance(balance int64) float32 {
	if balance == 0 {
		return 0
	}

	return float32(balance) / 1_000_000_000
}

func miningCumulativeRewards(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, client *http.Client) services.MessageHandler {
	printer := message.NewPrinter(language.English)

	return func(ctx context.Context, message *tgbotapi.Message) error {
		splitText := strings.Split(message.Text, " ")
		cryptoCode, accountNumber := parseMiningPair(splitText[1])
		cryptoCode = strings.ToUpper(cryptoCode)
		currencyCode := "IDR"

		wg := sync.WaitGroup{}
		twoMinersStatsChan := make(chan twoMinersStatsResponse, 1)
		indodaxTickerChan := make(chan indodaxTickerResponse, 1)
		errChan := make(chan error, 2)

		wg.Add(1)
		go func() {
			defer wg.Done()

			stats, err := getTwoMinersStats(client, cryptoCode, accountNumber)
			if err != nil {
				msg := tgbotapi.NewMessage(message.Chat.ID, err.Error())
				_, sendErr := bot.Send(msg)
				if sendErr != nil {
					logger.WithError(sendErr).Error("miner.miningOverallStats")
				}

				errChan <- err
				return
			}

			twoMinersStatsChan <- stats
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()

			ticker, err := getIndodaxTicker(client, cryptoCode, currencyCode)
			if err != nil {
				msg := tgbotapi.NewMessage(message.Chat.ID, err.Error())
				_, sendErr := bot.Send(msg)
				if sendErr != nil {
					logger.WithError(sendErr).Error("miner.miningOverallStats")
				}

				errChan <- err
				return
			}

			indodaxTickerChan <- ticker
		}()

		wg.Wait()
		if len(errChan) > 0 {
			return <-errChan
		}
		stats := <-twoMinersStatsChan
		ticker := <-indodaxTickerChan

		text := fmt.Sprintf("*CUMULATIVE %s MINING REWARDS*", cryptoCode)
		for _, reward := range stats.SumRewards {
			rewardInIDR := printer.Sprintf("%.0f %s", convertBalance(reward.Reward * ticker.Data.LastPrice), currencyCode)

			text += fmt.Sprintf("\n\n*%s*\n", reward.Name)
			text += fmt.Sprintf("Reward: %f %s | %s\n", convertBalance(reward.Reward), cryptoCode, rewardInIDR)
			text += fmt.Sprintf("Count: %d", reward.Count)
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err := bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("miner.miningOverallStats")
		}

		return nil
	}
}

func miningRecentRewards(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, client *http.Client) services.MessageHandler {
	printer := message.NewPrinter(language.English)

	return func(ctx context.Context, message *tgbotapi.Message) error {
		splitText := strings.Split(message.Text, " ")
		cryptoCode, accountNumber := parseMiningPair(splitText[1])
		cryptoCode = strings.ToUpper(cryptoCode)
		currencyCode := "IDR"

		wg := sync.WaitGroup{}
		twoMinersStatsChan := make(chan twoMinersStatsResponse, 1)
		indodaxTickerChan := make(chan indodaxTickerResponse, 1)
		errChan := make(chan error, 2)

		wg.Add(1)
		go func() {
			defer wg.Done()

			stats, err := getTwoMinersStats(client, cryptoCode, accountNumber)
			if err != nil {
				msg := tgbotapi.NewMessage(message.Chat.ID, err.Error())
				_, sendErr := bot.Send(msg)
				if sendErr != nil {
					logger.WithError(sendErr).Error("miner.miningOverallStats")
				}

				errChan <- err
				return
			}

			twoMinersStatsChan <- stats
		}()

		wg.Add(1)
		go func() {
			defer wg.Done()

			ticker, err := getIndodaxTicker(client, cryptoCode, currencyCode)
			if err != nil {
				msg := tgbotapi.NewMessage(message.Chat.ID, err.Error())
				_, sendErr := bot.Send(msg)
				if sendErr != nil {
					logger.WithError(sendErr).Error("miner.miningOverallStats")
				}

				errChan <- err
				return
			}

			indodaxTickerChan <- ticker
		}()

		wg.Wait()
		if len(errChan) > 0 {
			return <-errChan
		}
		stats := <-twoMinersStatsChan
		ticker := <-indodaxTickerChan

		if len(stats.Rewards) > 10 {
			stats.Rewards = stats.Rewards[:10]
		}

		text := fmt.Sprintf("*RECENT %s MINING REWARDS*\n", cryptoCode)
		for i, reward := range stats.Rewards {
			rewardInIDR := printer.Sprintf("%.0f %s", convertBalance(reward.Reward * ticker.Data.LastPrice), currencyCode)

			text += fmt.Sprintf("\n*%d. %s*\n", i+1, reward.Timestamp.In(currentTimezone).Format(time.RFC1123))
			text += fmt.Sprintf("Reward: %f %s | %s\n", convertBalance(reward.Reward), cryptoCode, rewardInIDR)
			text += fmt.Sprintf("Percentage: %f%%\n", reward.Percentage)

			flags := make([]string, 0)
			if reward.Immature {
				flags = append(flags, "Immature")
			}
			if reward.Orphan {
				flags = append(flags, "Orphan")
			}
			if reward.Uncle {
				flags = append(flags, "Uncle")
			}

			if len(flags) > 0 {
				text += fmt.Sprintf("%s\n", strings.Join(flags, ", "))
			}
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err := bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("miner.miningOverallStats")
		}

		return nil
	}
}

func miningWorkerStats(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, client *http.Client) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		splitText := strings.Split(message.Text, " ")
		cryptoCode, accountNumber := parseMiningPair(splitText[1])
		cryptoCode = strings.ToUpper(cryptoCode)

		stats, err := getTwoMinersStats(client, cryptoCode, accountNumber)
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, err.Error())
			_, sendErr := bot.Send(msg)
			if sendErr != nil {
				logger.WithError(sendErr).Error("miner.miningWorkerStats")
			}

			return err
		}

		text := fmt.Sprintf("*%s MINING WORKERS STATS*", cryptoCode)
		for worker, stats := range stats.Workers {
			status := "Online"
			if stats.IsOffline {
				status = "Offline"
			}

			text += fmt.Sprintf("\n\n*%s (%s)*\n", worker, status)
			text += fmt.Sprintf("Current Hash Rate: %0.2f MH/s\n", convertHashRate(stats.CurrentHashRate))
			text += fmt.Sprintf("Avg Hash Rate: %0.2f MH/s\n", convertHashRate(stats.AvgHashRate))
			text += fmt.Sprintf("Last beat at %s", stats.LastBeat.In(currentTimezone).Format(time.RFC1123))
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err = bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("miner.miningWorkerStats")
		}

		return nil
	}
}

func convertHashRate(hashRate int32) float32 {
	return float32(hashRate) / 1_000_000
}
