package miner

const overallStatsMessage = `*OVERALL {cryptoCode} MINING STATS*

	*Last 24h Reward*
	{24hReward} {cryptoCode} | {24hRewardIDR} {currencyCode}

	*Unpaid Balance*
	{unpaidBalance} {cryptoCode} | {unpaidBalanceIDR} {currencyCode}

	*Immature Balance*
	{immatureBalance} {cryptoCode} | {immatureBalanceIDR} {currencyCode}

	*Total Paid*
	{paidBalance} {cryptoCode} | {paidBalanceIDR} {currencyCode}

	Last share at {lastShare}`
