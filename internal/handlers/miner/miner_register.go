package miner

import (
	"net/http"
	"os"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/dewadg/sadalsuud/internal/handlers/middlewares"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

var currentTimezone *time.Location

func Register(
	botSvc services.BotService,
	bot *tgbotapi.BotAPI,
	logger logrus.FieldLogger,
	godMap map[string]bool,
) {
	miningListString := strings.Split(os.Getenv("2MINERS_MINING_LIST"), ",")
	miningListMap := make(map[string][]string)
	for _, miningPair := range miningListString {
		splitMiningPair := strings.Split(miningPair, ":")
		cryptoCode := splitMiningPair[0]
		accounts := strings.Split(splitMiningPair[1], ";")

		miningListMap[cryptoCode] = accounts
	}

	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	tz, _ := time.LoadLocation("Asia/Jakarta")
	currentTimezone = tz

	godMiddleware := middlewares.GodOnlyMiddleware(godMap)

	botSvc.On("/mnLs", middlewares.Chain(miningList(bot, logger, miningListMap), godMiddleware))
	botSvc.On("/mnOverallStats", middlewares.Chain(miningOverallStats(bot, logger, client), godMiddleware))
	botSvc.On("/mnCumulativeRewards", middlewares.Chain(miningCumulativeRewards(bot, logger, client), godMiddleware))
	botSvc.On("/mnRecentRewards", middlewares.Chain(miningRecentRewards(bot, logger, client), godMiddleware))
	botSvc.On("/mnWorkerStats", middlewares.Chain(miningWorkerStats(bot, logger, client), godMiddleware))
}
