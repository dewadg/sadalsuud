package middlewares

import (
	"context"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

func Chain(handler services.MessageHandler, middlewares ...services.MessageHandler) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		for _, middleware := range middlewares {
			if err := middleware(ctx, message); err != nil {
				return nil
			}
		}

		return handler(ctx, message)
	}
}
