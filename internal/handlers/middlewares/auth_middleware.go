package middlewares

import (
	"context"
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

func GodOnlyMiddleware(gods map[string]bool) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		if _, ok := gods[message.From.UserName]; !ok {
			return fmt.Errorf("%s: no_access_given", message.From.UserName)
		}
		return nil
	}
}
