package crypto

import (
	"context"
)

type cryptoRepository interface {
	GetBalance(ctx context.Context) ([]cryptoBalance, error)

	GetTicker(ctx context.Context, pair string) (indodaxTickerResponse, error)
}

type cryptoWatchListRepository interface {
	Get(ctx context.Context) ([]string, error)

	Add(ctx context.Context, pair string) error

	Remove(ctx context.Context, pair string) error
}
