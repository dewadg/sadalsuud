package crypto

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

type indodaxCryptoRepository struct {
	apiHost   string
	apiKey    string
	secretKey string
	client    *http.Client
}

type indodaxGetInfoResponse struct {
	Data struct {
		Balance     map[string]interface{} `json:"balance"`
		BalanceHold map[string]interface{} `json:"balance_hold"`
	} `json:"return"`
}

type indodaxErrorResponse struct {
	Error            string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

type indodaxTickerResponse struct {
	Pair string
	Data struct {
		LastPriceString string  `json:"last"`
		LastPrice       float64 `json:"-"`
	} `json:"ticker"`
}

func newIndodaxCryptoRepository(apiKey string, secretKey string) cryptoRepository {
	return &indodaxCryptoRepository{
		apiHost:   "https://indodax.com",
		apiKey:    apiKey,
		secretKey: secretKey,
		client: &http.Client{
			Timeout: 5 * time.Second,
		},
	}
}

func (repo *indodaxCryptoRepository) GetBalance(ctx context.Context) ([]cryptoBalance, error) {
	requestBody := url.Values{}
	requestBody.Set("method", "getInfo")
	requestBody.Set("timestamp", fmt.Sprintf("%d", time.Now().UnixNano()/int64(time.Millisecond)))
	requestBody.Set("recvWindow", fmt.Sprintf("%d", time.Now().Add(5*time.Second).UnixNano()/int64(time.Millisecond)))

	request, err := http.NewRequest(http.MethodPost, repo.apiHost+"/tapi", bytes.NewBuffer([]byte(requestBody.Encode())))
	if err != nil {
		return nil, err
	}
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("Key", repo.apiKey)
	request.Header.Set("Sign", repo.generateSign(requestBody.Encode()))

	response, err := repo.client.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	responseBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var errorResponseBody indodaxErrorResponse
	if err := json.Unmarshal(responseBytes, &errorResponseBody); err == nil && errorResponseBody.Error != "" {
		return nil, fmt.Errorf("indodax_call_failed_with_error: %s", errorResponseBody.Error)
	}

	var responseBody indodaxGetInfoResponse
	if err := json.Unmarshal(responseBytes, &responseBody); err != nil {
		return nil, err
	}

	balances := make([]cryptoBalance, 0)
	pairs := make([]string, 0)
	for cryptoCode, value := range responseBody.Data.Balance {
		if strings.ToLower(cryptoCode) == "idr" {
			continue
		}
		cryptoValue, err := repo.parsePrice(value)
		if err != nil {
			return nil, err
		}
		if cryptoValue == 0 {
			continue
		}

		balance := cryptoBalance{
			CryptoCode:    strings.ToUpper(cryptoCode),
			CurrencyCode:  "",
			CryptoValue:   cryptoValue,
			CurrencyValue: 0,
			IsHold:        false,
		}

		balances = append(balances, balance)
		pairs = append(pairs, cryptoCode+"idr")
	}
	for cryptoCode, value := range responseBody.Data.BalanceHold {
		if strings.ToLower(cryptoCode) == "idr" {
			continue
		}
		cryptoValue, err := repo.parsePrice(value)
		if err != nil {
			return nil, err
		}
		if cryptoValue == 0 {
			continue
		}

		balance := cryptoBalance{
			CryptoCode:    strings.ToUpper(cryptoCode),
			CurrencyCode:  "",
			CryptoValue:   cryptoValue,
			CurrencyValue: 0,
			IsHold:        true,
		}

		balances = append(balances, balance)
		pairs = append(pairs, cryptoCode+"idr")
	}

	tickersMap, err := repo.getTickersMap(ctx, pairs)
	if err != nil {
		return nil, err
	}

	for i := range balances {
		ticker, _ := tickersMap[strings.ToLower(balances[i].CryptoCode)+"idr"]

		balances[i].CurrencyCode = "IDR"
		balances[i].CurrencyValue = ticker.Data.LastPrice * balances[i].CryptoValue
	}

	return balances, nil
}

func (repo *indodaxCryptoRepository) parsePrice(value interface{}) (float64, error) {
	if floatValue, floatOk := value.(float64); floatOk {
		return floatValue, nil
	}

	if stringValue, stringOk := value.(string); stringOk {
		cryptoValueInFloat64, err := strconv.ParseFloat(stringValue, 64)
		if err != nil {
			return 0, err
		}
		return cryptoValueInFloat64, nil
	}

	return 0, nil
}

func (repo *indodaxCryptoRepository) generateSign(value string) string {
	signer := hmac.New(sha512.New, []byte(repo.secretKey))
	signer.Write([]byte(value))

	return hex.EncodeToString(signer.Sum(nil))
}

func (repo *indodaxCryptoRepository) getTickersMap(ctx context.Context, pairs []string) (map[string]indodaxTickerResponse, error) {
	wg := sync.WaitGroup{}
	wg.Add(len(pairs))

	tickerChan := make(chan indodaxTickerResponse, len(pairs))
	errChan := make(chan error, len(pairs))
	doneChan := make(chan interface{})
	for _, pair := range pairs {
		go func(pair string) {
			defer wg.Done()

			ticker, err := repo.GetTicker(ctx, pair)
			if err != nil {
				errChan <- err
				return
			}

			tickerChan <- ticker
		}(pair)
	}

	go func() {
		wg.Wait()
		doneChan <- nil
	}()

	tickersMap := make(map[string]indodaxTickerResponse)
	for {
		select {
		case err := <-errChan:
			return nil, err
		case <-doneChan:
			return tickersMap, nil
		case ticker := <-tickerChan:
			tickersMap[ticker.Pair] = ticker
		}
	}
}

func (repo *indodaxCryptoRepository) GetTicker(ctx context.Context, pair string) (indodaxTickerResponse, error) {
	request, err := http.NewRequest(http.MethodGet, repo.apiHost+"/api/ticker/"+strings.ToLower(pair), nil)
	if err != nil {
		return indodaxTickerResponse{}, err
	}

	response, err := repo.client.Do(request)
	if err != nil {
		return indodaxTickerResponse{}, err
	}
	defer response.Body.Close()

	responseBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return indodaxTickerResponse{}, err
	}

	var errorResponseBody indodaxErrorResponse
	if err := json.Unmarshal(responseBytes, &errorResponseBody); err == nil && errorResponseBody.ErrorDescription != "" {
		return indodaxTickerResponse{}, errors.New(errorResponseBody.ErrorDescription)
	}

	var responseBody indodaxTickerResponse
	if err := json.Unmarshal(responseBytes, &responseBody); err != nil {
		return indodaxTickerResponse{}, err
	}

	if responseBody.Data.LastPriceString != "" {
		lastPrice, err := strconv.ParseFloat(responseBody.Data.LastPriceString, 64)
		if err != nil {
			return indodaxTickerResponse{}, err
		}
		responseBody.Data.LastPrice = lastPrice
	}

	responseBody.Pair = pair

	return responseBody, nil
}
