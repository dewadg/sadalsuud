package crypto

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"sync"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/dewadg/sadalsuud/internal/services"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

func cryptoList(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, repo cryptoRepository) services.MessageHandler {
	printer := message.NewPrinter(language.English)

	return func(ctx context.Context, message *tgbotapi.Message) error {
		balances, err := repo.GetBalance(ctx)
		if err != nil {
			logger.WithError(err).Error("crypto.cryptoList")
			return nil
		}

		var rows string
		var totalBalance float64
		for i, balance := range balances {
			if balance.IsHold {
				rows += fmt.Sprintf("\n*%d. %s/%s (On Hold)*\n", i+1, balance.CryptoCode, balance.CurrencyCode)
			} else {
				if balance.CryptoCode == "IDR" {
					rows += fmt.Sprintf("\n*%d. %s*\n", i+1, balance.CryptoCode)
				} else {
					rows += fmt.Sprintf("\n*%d. %s/%s*\n", i+1, balance.CryptoCode, balance.CurrencyCode)
				}

			}

			if balance.CryptoCode == "IDR" {
				rows += fmt.Sprintf("%s %s\n", printer.Sprintf("%0.f", balance.CryptoValue), balance.CryptoCode)
			} else {
				rows += fmt.Sprintf("%f %s | %s %s\n", balance.CryptoValue, balance.CryptoCode, printer.Sprintf("%0.f", balance.CurrencyValue), balance.CurrencyCode)
			}
			totalBalance += balance.CurrencyValue
		}

		text := fmt.Sprintf("*CRYPTO LIST*\n")
		text += "\n*Asset Value: " + printer.Sprintf("%0.f", totalBalance) + " IDR*\n"
		text += rows

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err = bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("crypto.cryptoList")
		}
		return nil
	}
}

func cryptoTicker(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, repo cryptoRepository) services.MessageHandler {
	printer := message.NewPrinter(language.English)

	return func(ctx context.Context, message *tgbotapi.Message) error {
		splitMsg := strings.Split(message.Text, " ")
		if len(splitMsg) < 2 {
			msg := tgbotapi.NewMessage(message.Chat.ID, "{pair} argument is needed. Example: /crTck ethidr")
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("crypto.cryptoTicker")
			}
			return nil
		}

		var value float64
		if len(splitMsg) >= 3 {
			parsedValue, err := strconv.ParseFloat(splitMsg[2], 32)
			if err != nil {
				msg := tgbotapi.NewMessage(message.Chat.ID, "{value} invalid. Example: /crTck ethidr 0.1234")
				msg.ParseMode = tgbotapi.ModeMarkdown
				_, err := bot.Send(msg)
				if err != nil {
					logger.WithError(err).Error("crypto.cryptoTicker")
				}
				return nil
			}

			value = parsedValue
		}

		pair := splitMsg[1]
		ticker, err := repo.GetTicker(ctx, pair)
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, "*Error while fetching crypto ticker:* "+err.Error())
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("crypto.cryptoTicker")
			}
			return nil
		}

		text := fmt.Sprintf("*%s:* %s\n", strings.ToUpper(ticker.Pair), printer.Sprintf("%0.f", ticker.Data.LastPrice))
		if value > 0 {
			text = fmt.Sprintf(
				"*%s:* %s (%s)\n",
				strings.ToUpper(ticker.Pair),
				printer.Sprintf("%0.f", ticker.Data.LastPrice),
				printer.Sprintf("%0.f", ticker.Data.LastPrice * value),
			)
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err = bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("crypto.cryptoTicker")
			return err
		}

		return nil
	}
}

func cryptoWatchList(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, wlRepo cryptoWatchListRepository, crRepo cryptoRepository) services.MessageHandler {
	printer := message.NewPrinter(language.English)

	return func(ctx context.Context, message *tgbotapi.Message) error {
		watchList, err := wlRepo.Get(ctx)
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, "*Error while fetching crypto watch list:* "+err.Error())
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("crypto.cryptoWatchList")
			}
			return nil
		}

		tickerMap, err := generateTickerMap(ctx, crRepo, watchList)
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, "*Error while fetching crypto watch list:* "+err.Error())
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("crypto.cryptoWatchList")
			}
			return nil
		}

		text := fmt.Sprintf("*CRYPTO WATCH LIST*\n\n")
		for i, pair := range watchList {
			text += fmt.Sprintf("*%d. %s*\n", i+1, strings.ToUpper(pair))
			if ticker, ok := tickerMap[pair]; ok {
				text += fmt.Sprintf("%s", printer.Sprintf("%0.f IDR\n\n", ticker.Data.LastPrice))
			} else {
				text += "0\n\n"
			}
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err = bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("crypto.cryptoWatchList")
			return err
		}

		return nil
	}
}

func generateTickerMap(ctx context.Context, crRepo cryptoRepository, pairs []string) (map[string]indodaxTickerResponse, error) {
	tickerChan := make(chan indodaxTickerResponse)
	errChan := make(chan error)
	doneChan := make(chan interface{})

	wg := sync.WaitGroup{}
	wg.Add(len(pairs))

	for _, pair := range pairs {
		go func(pair string) {
			defer wg.Done()

			ticker, err := crRepo.GetTicker(ctx, pair)
			if err != nil {
				errChan <- err
				return
			}

			tickerChan <- ticker
		}(pair)
	}

	go func() {
		wg.Wait()

		doneChan <- nil
	}()

	tickerMap := make(map[string]indodaxTickerResponse)
	for {
		select {
		case err := <-errChan:
			return nil, err
		case ticker := <-tickerChan:
			tickerMap[ticker.Pair] = ticker
			break
		case <-doneChan:
			return tickerMap, nil
		}
	}
}

func cryptoWatchListAdd(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, wlRepo cryptoWatchListRepository, crRepo cryptoRepository) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		splitMsg := strings.Split(message.Text, " ")
		if len(splitMsg) < 2 {
			msg := tgbotapi.NewMessage(message.Chat.ID, "{pair} argument is needed. Example: /crWlsAdd ethidr")
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("crypto.cryptoWatchListAdd")
			}
			return nil
		}

		pair := splitMsg[1]
		_, err := crRepo.GetTicker(ctx, pair)
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, "*Error while fetching ticker:* "+err.Error())
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("crypto.cryptoWatchListAdd")
			}
			return nil
		}

		if err := wlRepo.Add(ctx, pair); err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, "*Error while adding pair:* "+err.Error())
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("crypto.cryptoWatchListAdd")
			}
			return nil
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, "*Successfully added to watch list:* "+strings.ToUpper(pair))
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err = bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("crypto.cryptoWatchListAdd")
		}

		return nil
	}
}

func cryptoWatchListRemove(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, wlRepo cryptoWatchListRepository, crRepo cryptoRepository) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		splitMsg := strings.Split(message.Text, " ")
		if len(splitMsg) < 2 {
			msg := tgbotapi.NewMessage(message.Chat.ID, "{pair} argument is needed. Example: /crWlsRm ethidr")
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("crypto.cryptoWatchListRemove")
			}
			return nil
		}

		pair := splitMsg[1]
		if err := wlRepo.Remove(ctx, pair); err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, "*Error while removing pair:* "+err.Error())
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("crypto.cryptoWatchListRemove")
			}
			return nil
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, "*Successfully removed from watch list:* "+strings.ToUpper(pair))
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err := bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("crypto.cryptoWatchListRemove")
		}

		return nil
	}
}
