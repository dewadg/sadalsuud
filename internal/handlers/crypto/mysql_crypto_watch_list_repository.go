package crypto

import (
	"context"
	"database/sql"
)

const (
	queryGetCryptoWatchList = `
		SELECT pair FROM crypto_watch_list
	`

	queryAddCryptoWatchList = `
		INSERT INTO crypto_watch_list SET pair = ?, created_at = NOW();
	`

	queryRemoveCryptoWatchList = `
		DELETE FROM crypto_watch_list WHERE pair = ?
	`
)

type mysqlCryptoWatchListRepository struct {
	db *sql.DB
}

func newMysqlCryptoWatchListRepository(db *sql.DB) cryptoWatchListRepository {
	return &mysqlCryptoWatchListRepository{
		db: db,
	}
}

func (repo *mysqlCryptoWatchListRepository) Get(ctx context.Context) ([]string, error) {
	rows, err := repo.db.Query(queryGetCryptoWatchList)
	if err != nil {
		return nil, err
	}

	watchList := make([]string, 0)
	for rows.Next() {
		var pair string
		if err := rows.Scan(&pair); err != nil {
			return nil, err
		}

		watchList = append(watchList, pair)
	}

	return watchList, nil
}

func (repo *mysqlCryptoWatchListRepository) Add(ctx context.Context, pair string) error {
	_, err := repo.db.ExecContext(ctx, queryAddCryptoWatchList, pair)
	return err
}

func (repo *mysqlCryptoWatchListRepository) Remove(ctx context.Context, pair string) error {
	_, err := repo.db.ExecContext(ctx, queryRemoveCryptoWatchList, pair)
	return err
}
