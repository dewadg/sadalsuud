package crypto

type cryptoBalance struct {
	CryptoCode    string
	CurrencyCode  string
	CryptoValue   float64
	CurrencyValue float64
	IsHold        bool
}
