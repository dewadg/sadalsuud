package crypto

import (
	"database/sql"
	"gitlab.com/dewadg/sadalsuud/internal/handlers/middlewares"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

func Register(
	botSvc services.BotService,
	bot *tgbotapi.BotAPI,
	logger logrus.FieldLogger,
	db *sql.DB,
	godMap map[string]bool,
) {
	cryptoRepo := newIndodaxCryptoRepository(os.Getenv("INDODAX_API_KEY"), os.Getenv("INDODAX_SECRET_KEY"))
	cryptoWatchListRepo := newMysqlCryptoWatchListRepository(db)

	godMiddleware := middlewares.GodOnlyMiddleware(godMap)

	botSvc.On("/crLs", middlewares.Chain(cryptoList(bot, logger, cryptoRepo), godMiddleware))
	botSvc.On("/crTck", cryptoTicker(bot, logger, cryptoRepo))
	botSvc.On("/crWls", middlewares.Chain(cryptoWatchList(bot, logger, cryptoWatchListRepo, cryptoRepo)))
	botSvc.On("/crWlsAdd", middlewares.Chain(cryptoWatchListAdd(bot, logger, cryptoWatchListRepo, cryptoRepo)))
	botSvc.On("/crWlsRm", middlewares.Chain(cryptoWatchListRemove(bot, logger, cryptoWatchListRepo, cryptoRepo)))
}
