package heda

import (
	"context"
	"errors"
	"fmt"
	"strconv"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/dewadg/heda/pkg/entities"
	"gitlab.com/dewadg/heda/pkg/keys"
	"gitlab.com/dewadg/heda/pkg/search"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

func searchProduct(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, hedaSvc search.Service) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		ctx = context.WithValue(ctx, keys.KeyUserAgent, "sadalsuud/1.0.0")

		splittedMsg := strings.Split(message.Text, " ")
		if len(splittedMsg) < 2 {
			msg := tgbotapi.NewMessage(message.Chat.ID, fmt.Sprintf("/searchProduct Please enter valid command (price flag is optional):\n /searchProduct coffeemaker price=100-200"))
			msg.ParseMode = tgbotapi.ModeMarkdown

			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("heda.searchProduct")
				return err
			}
		}

		params, err := parseSearchProductParams(splittedMsg[1:])
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, fmt.Sprintf("/searchProduct Please enter valid command (price flag is optional):\n /searchProduct coffeemaker price=100-200"))
			msg.ParseMode = tgbotapi.ModeMarkdown

			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("heda.searchProduct")
				return err
			}
		}
		logger = logger.WithField("param", params)

		var resultErr *search.ResultError
		result, err := hedaSvc.Search(ctx, params)
		if err != nil {
			if e, ok := err.(*search.ResultError); ok {
				resultErr = e
			} else {
				logger.WithError(err).Error("heda.searchProduct")

				msg := tgbotapi.NewMessage(message.Chat.ID, fmt.Sprintf("/searchProduct Error: %s", err.Error()))
				bot.Send(msg)

				return err
			}
		}

		for i, item := range result.Items {
			text := fmt.Sprintf("*%d. %s*\n", i+1, item.Name)
			text += fmt.Sprintf("Source: %s\n", item.Source)
			text += fmt.Sprintf("Price: IDR %d\n", item.Price)
			text += fmt.Sprintf("URL: %s", item.URL)

			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			msg.ParseMode = tgbotapi.ModeMarkdown
			msg.DisableNotification = true
			msg.DisableWebPagePreview = true

			_, err = bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("heda.searchProduct")
			}
		}

		if resultErr != nil {
			text := fmt.Sprintf("*Errors*\n")
			for _, errItem := range resultErr.All() {
				text += fmt.Sprintf("%s\n", strings.Replace(errItem.Error(), "_", "\\_", -1))
			}

			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			msg.ParseMode = tgbotapi.ModeMarkdown
			msg.DisableNotification = true

			_, err = bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("heda.searchProduct")
			}
		}

		return nil
	}
}

func parseSearchProductParams(splittedMsg []string) (entities.SearchParams, error) {
	var params entities.SearchParams

	actualKeyword := make([]string, 0)
	for _, text := range splittedMsg {
		splittedText := strings.Split(text, "=")
		if len(splittedText) < 2 {
			actualKeyword = append(actualKeyword, text)
			continue
		}

		if splittedText[0] == "price" {
			splittedPriceFlags := strings.Split(splittedText[1], "-")
			if len(splittedPriceFlags) > 0 {
				parsedPrice, err := strconv.Atoi(splittedPriceFlags[0])
				if err != nil {
					return entities.SearchParams{}, errors.New("Invalid price flag")
				}

				params.PriceMin = int64(parsedPrice)
			}
			if len(splittedPriceFlags) > 1 {
				parsedPrice, err := strconv.Atoi(splittedPriceFlags[1])
				if err != nil {
					return entities.SearchParams{}, errors.New("Invalid price flag")
				}

				params.PriceMax = int64(parsedPrice)
			}
		}
	}

	params.Name = strings.Join(actualKeyword, " ")

	return params, nil
}
