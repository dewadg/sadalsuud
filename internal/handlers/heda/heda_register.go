package heda

import (
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/dewadg/heda/pkg/config"
	"gitlab.com/dewadg/heda/pkg/search"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

func Register(
	botSvc services.BotService,
	bot *tgbotapi.BotAPI,
	logger logrus.FieldLogger,
) {
	hedaCfg := &config.Config{UserHomeDir: os.Getenv("HEDA_USER_HOME_DIR")}
	hedaSvc := search.New(hedaCfg, []string{"tokopedia", "blibli", "bukalapak", "shopee"})

	botSvc.On("/searchProduct", searchProduct(bot, logger, hedaSvc))
}
