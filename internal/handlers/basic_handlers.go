package handlers

import (
	"context"
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

func LogMessage(logger logrus.FieldLogger) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		logger.
			WithFields(logrus.Fields{
				"username":  message.Chat.UserName,
				"timestamp": message.Time().UTC(),
				"message":   message.Text,
			}).
			Info("incoming_message")

		return nil
	}
}

func Help(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, godMap map[string]bool) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		commands := RegularCommands
		if _, isGod := godMap[message.From.UserName]; isGod {
			commands = GodCommands
		}

		text := "*AVAILABLE COMMANDS*\n\n"
		count := 1
		for command, desc := range commands {
			text += fmt.Sprintf("%d. %s: %s\n", count, command, desc)
			count++
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err := bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("ServiceInspect")
		}
		return err
	}
}
