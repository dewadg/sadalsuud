package swarm

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/dewadg/sadalsuud/internal/handlers/middlewares"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

func Register(
	botSvc services.BotService,
	bot *tgbotapi.BotAPI,
	logger logrus.FieldLogger,
	godMap map[string]bool,
) {
	swarmSvc := newSwarmService()

	godMiddleware := middlewares.GodOnlyMiddleware(godMap)

	botSvc.On("/stackLs", middlewares.Chain(stackLs(bot, swarmSvc), godMiddleware))
	botSvc.On("/serviceLs", middlewares.Chain(serviceLs(bot, swarmSvc), godMiddleware))
	botSvc.On("/serviceInspect", middlewares.Chain(serviceInspect(bot, logger, swarmSvc), godMiddleware))
	botSvc.On("/serviceLogs", middlewares.Chain(serviceLogs(bot, logger, swarmSvc), godMiddleware))
	botSvc.On("/serviceRestart", middlewares.Chain(serviceRestart(bot, logger, swarmSvc), godMiddleware))
}
