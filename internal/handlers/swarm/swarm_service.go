package swarm

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"os/exec"
	"strings"

	"gitlab.com/dewadg/sadalsuud/internal/models"
)

var defaultLogCount = 10
var defaultLogSince = "5m"

type swarmService struct{}

func newSwarmService() *swarmService {
	return &swarmService{}
}

func (svc *swarmService) GetStacks(ctx context.Context) ([]models.Stack, error) {
	var result bytes.Buffer
	cmd := exec.Command("docker", "stack", "ls", "--format='{{json .}}'")
	cmd.Stdout = &result
	if err := cmd.Run(); err != nil {
		return nil, err
	}

	stacks := make([]models.Stack, 0)
	for _, item := range strings.Split(result.String(), "\n") {
		item = strings.TrimPrefix(item, "'")
		item = strings.TrimSuffix(item, "'")

		var stack models.Stack
		if err := json.Unmarshal([]byte(item), &stack); err != nil {
			continue
		}

		stacks = append(stacks, stack)
	}

	return stacks, nil
}

func (svc *swarmService) GetServices(ctx context.Context) ([]models.Service, error) {
	var result bytes.Buffer
	cmd := exec.Command("docker", "service", "ls", "--format='{{json .}}'")
	cmd.Stdout = &result
	if err := cmd.Run(); err != nil {
		return nil, err
	}

	services := make([]models.Service, 0)
	for _, item := range strings.Split(result.String(), "\n") {
		item = strings.TrimPrefix(item, "'")
		item = strings.TrimSuffix(item, "'")

		var service models.Service
		if err := json.Unmarshal([]byte(item), &service); err != nil {
			continue
		}

		services = append(services, service)
	}

	return services, nil
}

func (svc *swarmService) InspectService(ctx context.Context, id string) (models.ServiceInspect, error) {
	var result bytes.Buffer
	cmd := exec.Command("docker", "service", "inspect", id, "--format='{{json .}}'")
	cmd.Stdout = &result
	if err := cmd.Run(); err != nil {
		return models.ServiceInspect{}, err
	}

	item := result.String()
	item = strings.TrimPrefix(item, "'")
	item = strings.TrimSuffix(item, "'")
	item = item[0 : len(item)-2]

	var serviceInspect models.ServiceInspect
	if err := json.Unmarshal([]byte(item), &serviceInspect); err != nil {
		return models.ServiceInspect{}, err
	}
	return serviceInspect, nil
}

func (svc *swarmService) GetServiceLogs(ctx context.Context, id string, filter models.ServiceLogFilter) ([]string, error) {
	if filter.Since == nil {
		filter.Since = &defaultLogSince
	}
	if filter.Tail == nil {
		filter.Tail = &defaultLogCount
	}

	args := []string{
		"service",
		"logs",
		id,
		fmt.Sprintf("--since=%s", *filter.Since),
		fmt.Sprintf("--tail=%d", *filter.Tail),
	}
	if filter.Until != nil {
		args = append(args, fmt.Sprintf("--until=%s", *filter.Until))
	}

	var result bytes.Buffer
	cmd := exec.Command("docker", args...)
	cmd.Stdout = &result
	if err := cmd.Run(); err != nil {
		return nil, err
	}
	splittedResult := strings.Split(result.String(), "\n")
	logs := make([]string, 0)
	for _, log := range splittedResult {
		log = strings.TrimSpace(log)
		if len(log) == 0 {
			continue
		}

		logs = append(logs, log)
	}

	return logs, nil
}

func (svc *swarmService) RestartService(ctx context.Context, id string) (<-chan string, error) {
	args := []string{
		"service",
		"update",
		id,
	}

	cmd := exec.Command("docker", args...)
	resultChan := make(chan string)
	pipe, err := cmd.StdoutPipe()
	if err != nil {
		return nil, err
	}
	if err := cmd.Start(); err != nil {
		return nil, err
	}
	reader := bufio.NewReader(pipe)

	go func() {
		var lineBefore string

		defer func() {
			cmd.Wait()
			close(resultChan)
		}()

		for {
			if err := ctx.Err(); err != nil {
				return
			}

			line, err := reader.ReadString('\n')
			if err != nil && (err == io.EOF || err == bufio.ErrFinalToken) {
				return
			}

			line = strings.TrimSpace(line)
			if len(line) == 0 || lineBefore == line {
				continue
			}

			lineBefore = line
			resultChan <- line
		}
	}()

	return resultChan, nil
}
