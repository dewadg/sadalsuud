package swarm

import (
	"fmt"
	"strconv"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/dewadg/sadalsuud/internal/models"
)

func parseServiceLogFilter(message *tgbotapi.Message) (models.ServiceLogFilter, error) {
	filter := models.ServiceLogFilter{}
	args := strings.Split(message.Text, " ")
	if len(args) < 3 {
		return models.ServiceLogFilter{}, nil
	}

	for i := 2; i < len(args); i++ {
		splittedMessage := strings.Split(args[i], "=")
		if len(splittedMessage) < 2 {
			continue
		}

		if splittedMessage[0] == "since" {
			filter.Since = &splittedMessage[1]
		}
		if splittedMessage[0] == "until" {
			filter.Until = &splittedMessage[1]
		}
		if splittedMessage[0] == "tail" {
			tail, err := strconv.Atoi(splittedMessage[1])
			if err != nil {
				return models.ServiceLogFilter{}, fmt.Errorf("Invalid value for argument `tail`: %s", splittedMessage[2])
			}

			filter.Tail = &tail
		}
	}

	return filter, nil
}
