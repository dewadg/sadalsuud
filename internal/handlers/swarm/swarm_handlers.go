package swarm

import (
	"context"
	"fmt"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

func stackLs(bot *tgbotapi.BotAPI, swarmSvc *swarmService) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		stacks, err := swarmSvc.GetStacks(ctx)
		if err != nil {
			return err
		}

		text := "*STACK LIST*\n\n"
		for i, stack := range stacks {
			text += fmt.Sprintf("%d. %s\n", i+1, strings.Replace(stack.Name, "_", "\\_", -1))
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err = bot.Send(msg)
		return err
	}
}

func serviceLs(bot *tgbotapi.BotAPI, swarmSvc *swarmService) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		services, err := swarmSvc.GetServices(ctx)
		if err != nil {
			return err
		}

		text := "*SERVICE LIST*\n\n"
		for i, service := range services {
			service.Name = strings.Replace(service.Name, "_", "\\_", -1)
			service.Image = strings.Replace(service.Image, "_", "\\_", -1)

			text += fmt.Sprintf("*%d. %s*\n", i+1, service.ID)
			text += fmt.Sprintf("Name: %s\n", service.Name)
			text += fmt.Sprintf("Image: %s\n", service.Image)
			text += fmt.Sprintf("Replicas: %s\n", service.Replicas)
			text += "\n"
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err = bot.Send(msg)
		return err
	}
}

func serviceInspect(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, swarmSvc *swarmService) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		splittedMessage := strings.Split(message.Text, " ")
		if len(splittedMessage) <= 1 {
			msg := tgbotapi.NewMessage(message.Chat.ID, "/serviceInspect Please specify service ID")
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceInspect")
			}

			return err
		}

		svcInspect, err := swarmSvc.InspectService(ctx, splittedMessage[1])
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, fmt.Sprintf("/serviceInspect Error: %s", err.Error()))
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceInspect")
			}

			return err
		}

		text := fmt.Sprintf("*SERVICE %s*\n\n", svcInspect.ID)
		text += fmt.Sprintf("Created at: %s UTC\n", svcInspect.CreatedAt.Format("2006-01-02 15:04:05"))
		text += fmt.Sprintf("Updated at: %s UTC\n", svcInspect.UpdatedAt.Format("2006-01-02 15:04:05"))
		text += fmt.Sprintf("Current version: %d\n\n", svcInspect.Version.Index)

		text += fmt.Sprintf("*SERVICE SPEC*\n\n")
		text += fmt.Sprintf("Name: %s\n", strings.Replace(svcInspect.Spec.Name, "_", "\\_", -1))
		text += fmt.Sprintf("Labels:\n")
		for key, value := range svcInspect.Spec.Labels {
			text += fmt.Sprintf("- %s: %s\n", key, value)
		}

		text += fmt.Sprintf("\n*CONTAINER SPEC*\n\n")
		text += fmt.Sprintf("Env:\n")
		for _, env := range svcInspect.Spec.TaskTemplate.ContainerSpec.Env {
			text += fmt.Sprintf("- %s\n", strings.Replace(env, "_", "\\_", -1))
		}
		text += fmt.Sprintf("\nLabels:\n")
		for key, value := range svcInspect.Spec.TaskTemplate.ContainerSpec.Labels {
			text += fmt.Sprintf("- %s: %s\n", key, strings.Replace(value, "_", "\\_", -1))
		}

		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		msg.ParseMode = tgbotapi.ModeMarkdown
		_, err = bot.Send(msg)
		if err != nil {
			logger.WithError(err).Error("serviceInspect")
		}

		return err
	}
}

func serviceLogs(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, swarmSvc *swarmService) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		splittedMessage := strings.Split(message.Text, " ")
		if len(splittedMessage) <= 1 {
			msg := tgbotapi.NewMessage(message.Chat.ID, "/serviceLogs Please specify service ID")
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceLogs")
			}

			return err
		}

		filter, err := parseServiceLogFilter(message)
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, fmt.Sprintf("/serviceLogs Error: %s", err.Error()))
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceLogs")
			}

			return err
		}

		serviceID := splittedMessage[1]
		logs, err := swarmSvc.GetServiceLogs(ctx, serviceID, filter)
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, fmt.Sprintf("/serviceLogs Error: %s", err.Error()))
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceLogs")
			}

			return err
		}
		if len(logs) == 0 {
			msg := tgbotapi.NewMessage(message.Chat.ID, "/serviceLogs No logs")
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceLogs")
			}

			return err
		}

		for _, log := range logs {
			text := "```\n"
			text += log + "\n"
			text += "```\n"

			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err = bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceLogs")
			}
		}

		return nil
	}
}

func serviceRestart(bot *tgbotapi.BotAPI, logger logrus.FieldLogger, swarmSvc *swarmService) services.MessageHandler {
	return func(ctx context.Context, message *tgbotapi.Message) error {
		splittedMessage := strings.Split(message.Text, " ")
		if len(splittedMessage) <= 1 {
			msg := tgbotapi.NewMessage(message.Chat.ID, "/serviceLogs Please specify service ID")
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceLogs")
			}

			return err
		}

		resultChan, err := swarmSvc.RestartService(ctx, splittedMessage[1])
		if err != nil {
			msg := tgbotapi.NewMessage(message.Chat.ID, fmt.Sprintf("/serviceLogs Error: %s", err.Error()))
			_, err := bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceLogs")
			}

			return err
		}

		for result := range resultChan {
			text := "```\n"
			text += result + "\n"
			text += "```\n"

			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			msg.ParseMode = tgbotapi.ModeMarkdown
			_, err = bot.Send(msg)
			if err != nil {
				logger.WithError(err).Error("serviceLogs")
			}
		}

		return nil
	}
}
