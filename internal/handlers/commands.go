package handlers

var GodCommands = map[string]string{
	"/start":                      "Should I explain?",
	"/stackLs":                    "List available stacks",
	"/serviceLs":                  "List available services",
	"/serviceInspect {serviceID}": "Inspect a service",
	"/serviceLogs {serviceID}":    "Get service logs",
	"/serviceRestart {serviceID}": "Restart a service",
	"/mnLs":                       "Display crypto mining list",
	"/crLs":                       "Display crypto list",
	"/crTck":                      "Display crypto pair ticker (price)",
	"/crWls":                      "Display crypto watch list",
	"/crWlsAdd":                   "Add pair to crypto watch list",
	"/crWlsRm":                    "Remove pair from crypto watch list",
}

var RegularCommands = map[string]string{
	"/start": "Should I explain?",
	"/crTck": "Display crypto pair ticker (price)",
}
