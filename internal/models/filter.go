package models

type ServiceLogFilter struct {
	Since *string
	Until *string
	Tail  *int
}
