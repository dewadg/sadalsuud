package models

import "time"

type Stack struct {
	Name         string
	Namespace    string
	Orchestrator string
	Services     string
}

type Service struct {
	ID       string
	Image    string
	Name     string
	Replicas string
}

type ServiceInspect struct {
	ID        string
	CreatedAt time.Time
	UpdatedAt time.Time
	Version   struct {
		Index int
	}
	Spec struct {
		Name         string
		Labels       map[string]string
		TaskTemplate struct {
			ContainerSpec struct {
				Env    []string
				Image  string
				Labels map[string]string
			}
		}
	}
}
