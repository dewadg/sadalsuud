package serve

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	_ "github.com/go-sql-driver/mysql"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/dewadg/sadalsuud/internal/handlers"
	"gitlab.com/dewadg/sadalsuud/internal/handlers/crypto"
	"gitlab.com/dewadg/sadalsuud/internal/handlers/miner"
	"gitlab.com/dewadg/sadalsuud/internal/handlers/swarm"
	"gitlab.com/dewadg/sadalsuud/internal/services"
)

var gracefulPeriod = 10 * time.Second

func Command(logger logrus.FieldLogger) *cobra.Command {
	return &cobra.Command{
		Use: "serve",
		Run: func(cmd *cobra.Command, args []string) {
			if os.Getenv("APP_ENV") != "production" {
				gracefulPeriod = 0
			}

			db, err := sql.Open("mysql", os.Getenv("MYSQL_DSN"))
			if err != nil {
				logger.WithError(err).Fatalln("MySQL database connection error")
			}

			bot, err := tgbotapi.NewBotAPI(os.Getenv("TELEGRAM_BOT_TOKEN"))
			if err != nil {
				logger.WithError(err).Fatalln("Telegram bot init error")
			}

			logger.
				WithField("telegram_username", bot.Self.UserName).
				WithField("telegram_first_name", bot.Self.FirstName).
				WithField("telegram_last_name", bot.Self.LastName).
				Info("Starting")

			botSvc := services.NewBotService(logger, bot)
			godMap := generateGodMap()

			// register basic handlers
			botSvc.On("message", handlers.LogMessage(logger))
			botSvc.On("/start", handlers.Help(bot, logger, godMap))
			botSvc.On("/help", handlers.Help(bot, logger, godMap))

			// register additional handlers
			swarm.Register(botSvc, bot, logger, godMap)
			miner.Register(botSvc, bot, logger, godMap)
			crypto.Register(botSvc, bot, logger, db, godMap)

			ctx, stop := context.WithCancel(context.Background())

			interrupt := make(chan os.Signal)
			signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM, syscall.SIGKILL)

			go func() {
				<-interrupt

				logger.Info("Shutting down")

				stop()
			}()

			if err := botSvc.Listen(ctx); err != nil {
				logger.WithError(err).Fatalln("Listener init error")
			}

			<-ctx.Done()
			logger.Info(fmt.Sprintf("Waiting for %d second(s) to complete tasks", gracefulPeriod/1000))
			time.Sleep(gracefulPeriod)
			logger.Info("Shut down completed")
		},
	}
}

func generateGodMap() map[string]bool {
	gods := strings.Split(os.Getenv("TELEGRAM_ROLE_GOD_USERS"), ",")
	godMap := make(map[string]bool)
	for _, god := range gods {
		godMap[god] = true
	}

	return godMap
}
